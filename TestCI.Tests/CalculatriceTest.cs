﻿using System;
using NUnit;
using NUnit.Framework;

namespace TestCI.Tests
{
    [TestFixture]
    public class CalculatriceTests
    {
        [Test()]
        public void additionDeuxNombresSuccesTest()
        {
            Calculatrice calc = new Calculatrice();
            int resultat = calc.additionDeuxNombres(2, 4);
            Assert.AreEqual(6, 6);
        }

        [Test()]
        public void additionDeuxNombresFailedTest()
        {
            Calculatrice calc = new Calculatrice();
            int resultat = calc.additionDeuxNombres(2, 4);
            Assert.AreEqual(6, 6);
        }
    }
}
